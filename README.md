# dokuencrypt 

Adds a encrypt/decrypt functionality to the [ckgedit](https://www.dokuwiki.org/plugin:ckgedit) editor using the [encrypted passwords plugin](https://www.dokuwiki.org/plugin:encryptedpasswords)

## Installation

```
git clone https://gitlab.com/apg-w/dokuencrypt.git
```
Upload `dokuencrypt` to your ckgedit ckeditor plugins directory.
On a typical install this will be `var/wwww/dokuwiki/lib/plugins/ckgedit/ckeditor/plugins`

Add `dokuencrypt` to your ckgedit config file `/var/www/dokuwiki/lib/plugins/ckgedit/conf/default.php` line 35 in mine

```php
$conf['extra_plugins'] = "Geshi,Msword,dokuencrypt"
```

Copy contents of `fix_title.js` to `/var/www/dokuwiki/conf/userscripts.js`
-This script fixes CKEditor inserterting linebreaks (`\\`) in encrypted text

Alternatively copy this one liner to the file:
```javascript
jQuery(document).ready(() => document.querySelectorAll('.encryptedpasswords').forEach(e => e.title = e.title.replace(/\\|\s/g, "")));
```
