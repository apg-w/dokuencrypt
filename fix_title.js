// kwilliams
// 2020-02-25
// CKGedit inserts a '\\' for new lines that must replace in order to properly decrypt
// place this function in /var/www/dokuwiki/conf/userscript.js

jQuery(document).ready(function () {
    let encryptedTitles = document.querySelectorAll('.encryptedpasswords');
    for (let i = 0; i < encryptedTitles.length; i++) {
        let oldTitle = encryptedTitles[i].title;
        let newTitle = oldTitle.replace(/\\|\s/g, "");

        encryptedTitles[i].title = newTitle;
    }
});

jQuery(document).ready(() => document.querySelectorAll('.encryptedpasswords').forEach(e => e.title = e.title.replace(/\\|\s/g, "")));
