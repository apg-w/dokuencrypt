// kwilliams
// 2020-02-24
// adds an encrypt button to ckeditor compatible with encryptedpasswords plugin
// https://www.dokuwiki.org/plugin:encryptedpasswords

CKEDITOR.plugins.add('dokuencrypt', {
    icons: 'dokuencrypt',
    init: function (editor) {

        editor.addCommand('encryptSelected', {
            exec: function (editor) {
                handleEncrypt(editor);
            }
        });

        editor.ui.addButton('dokuencrypt', {
            label: 'Encrypt text',
            command: 'encryptSelected',
        });
    }
});

// handles encrypt and decrypt text
function handleEncrypt(editor) {
    var sample = editor.getSelection().getSelectedText();

    if (sample == '') {
        alert(LANG.plugins.encryptedpasswords['noSelection']);
        return false;
    }
    if (sample.indexOf('<decrypt>') == 0 && sample.indexOf('</decrypt>') == sample.length - 10) {
        vcPrompt(LANG.plugins.encryptedpasswords['enterKey'], LANG.plugins.encryptedpasswords['decrypt'], 1, vcFunc = function (a) {
            if (a) {
                document.getElementById('wiki__text').focus();
                try {
                    decText = GibberishAES.dec((sample.substr(9, sample.length - 19)), passElt.value);
                } catch (err) { decText = null }
                if (decText) {
                    editor.insertText(decText);
                    vcClick_func(0);
                    decText = null;
                } else {
                    alert(LANG.plugins.encryptedpasswords['invalidKey'])
                }
            } else {
                vcClick_func(0);
                document.getElementById('wiki__text').focus();
            };
        });
    } else {
        vcPrompt(LANG.plugins.encryptedpasswords['encryptKey'], LANG.plugins.encryptedpasswords['encrypt'], 2, vcFunc = function (a) {
            if (a) {
                if (passElt.value !== passElt2.value) {
                    alert(LANG.plugins.encryptedpasswords['keyErr']);
                    return false;
                }
                if (passElt.value == '') {
                    alert(LANG.plugins.encryptedpasswords['emptyKey']);
                    return false;
                }
                document.getElementById('wiki__text').focus();
                let encryptedText = GibberishAES.enc(sample, passElt.value).replace(/\n$|\r$|\r\n$|<br \>$/g, '');
                editor.insertText('<decrypt>' + encryptedText + '</decrypt>')
                vcClick_func(0);
            } else {
                vcClick_func(0);
                document.getElementById('wiki__text').focus();
            };
        });

    }
    return false;

}